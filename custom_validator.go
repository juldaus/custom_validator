package custom_validator

import (
	"fmt"
	"log"

	"github.com/go-playground/locales/en"
	ut "github.com/go-playground/universal-translator"
	"github.com/go-playground/validator/v10"
	en_translations "github.com/go-playground/validator/v10/translations/en"
)

type Add struct {
	Tag      string
	Function validator.Func
	Errmsg   string
}

func Validate(in interface{}, newvalidate []Add) (string, error) {
	var validate *validator.Validate
	var uni *ut.UniversalTranslator

	en := en.New()
	uni = ut.New(en, en)
	trans, _ := uni.GetTranslator("en")

	validate = validator.New()
	en_translations.RegisterDefaultTranslations(validate, trans)

	if newvalidate != nil {
		if err := RegisterCustomValidation(newvalidate, validate, trans); err != nil {
			log.Panicln(err)
		}
	}

	err := validate.Struct(in)
	if err != nil {
		errs := err.(validator.ValidationErrors)
		var erS []string
		for i, e := range errs {

			erS = append(erS, fmt.Sprint(i+1)+")."+e.Translate(trans))
		}
		return fmt.Sprint(erS), errs
	}

	return "", nil
}

func RegisterCustomValidation(in []Add, validate *validator.Validate, trans ut.Translator) error {
	for _, v := range in {
		if err := validate.RegisterValidation(v.Tag, v.Function); err != nil {
			return err
		}
		if err := validate.RegisterTranslation(v.Tag, trans, registerTranslator(v.Tag, v.Errmsg), translate); err != nil {
			return err
		}
	}
	return nil
}

func registerTranslator(tag, msg string) validator.RegisterTranslationsFunc {
	return func(ut ut.Translator) error {
		if err := ut.Add(tag, msg, false); err != nil {
			return err
		}
		return nil
	}
}

func translate(trans ut.Translator, fe validator.FieldError) string {
	msg, err := trans.T(fe.Tag(), fe.Field())
	if err != nil {
		panic(fe.(error).Error())
	}
	return msg
}

// wadaw
